export const environment = {
  production: true
};
export const serverUrl = 'https://werushtesttask-default-rtdb.europe-west1.firebasedatabase.app';
export const AVAILABLE_LOCALES = ['en', 'ru'];
export const DEFAULT_LOCALE = 'en';
