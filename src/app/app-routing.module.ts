import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AVAILABLE_LOCALES, DEFAULT_LOCALE} from '../environments/environment';

const routes: Routes = [
  {
    path: ':lang',
    loadChildren: () => import('./content/content.module').then(m => m.ContentModule)
  },
  {
    path: '',
    pathMatch: 'full',
    redirectTo: handleLocalization()
  }
];

function handleLocalization(): string {
  const browserLanguage = navigator.language.slice(0, 2);
  return AVAILABLE_LOCALES.includes(browserLanguage) ? browserLanguage : DEFAULT_LOCALE;
}

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
