import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {IGame} from '../model/interface/IGame';
import {serverUrl} from '../../../environments/environment';


@Injectable()
export class GameService {

  constructor(private http: HttpClient) {
  }

  get(): Observable<IGame[]> {
    return this.http.get<IGame[]>(`${serverUrl}/games.json`);
  }
}
