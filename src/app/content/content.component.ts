import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {GameService} from './service/game.service';
import {IGame} from './model/interface/IGame';
import {ILocalizedField} from './model/interface/ILocalizedField';
import {AVAILABLE_LOCALES, DEFAULT_LOCALE} from '../../environments/environment';
import {ActivatedRoute, Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ContentComponent implements OnInit {
  gameList: IGame[];
  categoryList: ILocalizedField[];
  isLoading: boolean;

  constructor(
    private gameService: GameService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private translateService: TranslateService
  ) {
    this.fixUrlLocaleParam();
  }

  getGames(): void {
    this.isLoading = true;
    this.gameService.get().subscribe(games => {
      this.gameList = games;
      this.getCategories(games);
      this.isLoading = false;
    }, error => {
      console.error(error);
      this.isLoading = false;
    });
  }

  getCategories(games: IGame[]): void {
    const categories = new Set<ILocalizedField>();
    for (const game of games) {
      categories.add(game.category);
    }
    this.categoryList = Array.from(categories);
  }

  fixUrlLocaleParam(): void {
    this.activatedRoute.params.subscribe(params => {
      const localeParam = params.lang;
      if (!localeParam || !AVAILABLE_LOCALES.includes(localeParam)) {
        const userLocale = navigator.language.slice(0, 2);
        const fixedLocale = AVAILABLE_LOCALES.includes(userLocale) ? userLocale : DEFAULT_LOCALE;
        this.translateService.use(fixedLocale);
        this.router.navigateByUrl(this.router.url.replace(localeParam, fixedLocale));
      }else {
        this.translateService.use(localeParam);
      }
    });
  }

  ngOnInit(): void {
    this.getGames();
  }


}






