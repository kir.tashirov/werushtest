import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {ContentRoutingModule} from './content-routing.module';
import {ContentComponent} from './content.component';
import {HttpClientModule} from '@angular/common/http';
import {GameService} from './service/game.service';
import {I18nModule} from '../i18n/i18n.module';

@NgModule({
  declarations: [
    ContentComponent
  ],
  imports: [
    CommonModule,
    ContentRoutingModule,
    HttpClientModule,
    I18nModule
  ],
  providers: [
    GameService
  ]

})
export class ContentModule {
}
