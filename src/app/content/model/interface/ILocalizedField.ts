export interface ILocalizedField {
  en: string;
  ru: string;
}
