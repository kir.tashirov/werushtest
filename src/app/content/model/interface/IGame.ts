import {ILocalizedField} from './ILocalizedField';

export interface IGame {
  id: number;
  name: ILocalizedField;
  category: ILocalizedField;
  image: string;
}
